extends Node

var user_file
var verified_user:bool = false

func _ready():
	load_game()

func save(response):
	var save_dict = {
		"name": response.user.name,
		"email": response.user.email,
		"username": response.user.username,
		"id": response.user._id,
		"token": response.token,
		"_id": response.user._id,
		"victories": response.user.victories,
		"losses": response.user.losses,
		"match": response.user.match
	}
	return save_dict

func save_game(response):
	var save_game = File.new()
	save_game.open("user://user.save", File.WRITE)
	save_game.store_line(to_json(save(response)))
	save_game.close()


func load_game():
	var save_game = File.new()
	var node_data
	if not save_game.file_exists("user://user.save"):
		return
	save_game.open("user://user.save", File.READ)
	while save_game.get_position() < save_game.get_len():
		# get the dictionary  from the next line in the save file
		node_data = parse_json(save_game.get_line())
		
		# Firstly, we need to create the object and add it to the
		# three and set its position
		
		# Now we set the remaining variables.
	
		user_file = node_data
	save_game.close()
	return node_data
