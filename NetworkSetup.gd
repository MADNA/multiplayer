extends Control

var player = load("res://Player.tscn")

var current_spawn_location_instance_number = 1
var current_player_for_spawn_location_number
var lobbyScene = load("res://Auth/Main.tscn")

onready var multiplayer_config_ui = $MultiplayerConfigure
onready var username_text_edit = $MultiplayerConfigure/UserNameTextEdit

onready var device_ip_address = $UI/DeviceIpAddress
onready var start_game = $UI/StartGame

func _ready() -> void:
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_to_server")

	$Tween.interpolate_property($MultiplayerConfigure, "modulate", Color(0, 0, 0, 0), Color(1, 1, 1, 1), 0.4)
	$Tween.start()
	device_ip_address.text = Network.ip_address
	$UI/BackToLobby.hide()
	
	$MultiplayerConfigure/UserName.text = User.user_file.name
	
	if get_tree().network_peer != null:
		multiplayer_config_ui.hide()
		if get_tree().is_network_server():
			$UI/BackToLobby.show()
		
		for player in PersistentNodes.get_children():
			if player.is_in_group("Player"):
				for spawn_location in $SpawnLocations.get_children():
					if int(spawn_location.name) == current_spawn_location_instance_number and current_player_for_spawn_location_number != player:
						player.rpc("update_position", spawn_location.global_position)
						player.rpc("enable")
						current_spawn_location_instance_number += 1
						current_player_for_spawn_location_number = player
	else:
		start_game.hide()

func _process(_delta) -> void:
	if get_tree().network_peer != null:
		if get_tree().get_network_connected_peers().size() >= 1 and get_tree().is_network_server():
			start_game.show()
			$ColorRect.hide()
		else:
			start_game.hide()
			$ColorRect.hide()

func _player_connected(id) -> void:
	print("Player " + str(id) + " has connected")
	
	instance_player(id)

func _player_disconnected(id) -> void:
	print("Player: " + str(id) + " has disconnected")
	
	if PersistentNodes.has_node(str(id)):
		PersistentNodes.get_node(str(id)).username_text_instance.queue_free()
		PersistentNodes.get_node(str(id)).queue_free()

func _on_CreateServer_pressed():
	#if username_text_edit.text != "":
	#	Network.current_player_username = username_text_edit.text
	Network.current_player_username = User.user_file.username
	multiplayer_config_ui.hide()
	$UI/BackToLobby.show()
	$ColorRect.hide()
	Network.create_server()
	instance_player(get_tree().get_network_unique_id())
	
	

func _on_JoinServer_pressed():
	# if username_text_edit.text != "":
		multiplayer_config_ui.hide()
		username_text_edit.hide()
		$ColorRect.hide()
		
		Global.instance_node(load("res://ServerBrowser.tscn"), self)

func _connected_to_server() -> void:
	yield(get_tree().create_timer(0.1), "timeout")
	instance_player(get_tree().get_network_unique_id())

func instance_player(id) -> void:
	var player_instance = Global.instance_node_at_location(player, PersistentNodes, get_node("SpawnLocations/" + str(current_spawn_location_instance_number)).global_position)
	player_instance.name = str(id)
	player_instance.set_network_master(id)
	# player_instance.username = username_text_edit.text
	player_instance.username = User.user_file.username
	current_spawn_location_instance_number += 1
	var player_id = {
		"id": id,
		"_id": player_instance.puppet_id
	}
	Global.players.insert(0, player_id)


func _on_StartGame_pressed():
	
	var headers = ["Content-Type: application/json", "token: " + User.user_file.token]
	
	var players = []
	
	for player in Global.players:
		players.push_back(player._id)
	
	var _query = {
		"name": "XD",
		"hoster": User.user_file._id,
		"players": players
	}
	
	var query = JSON.print(_query)
	
	print(players)
	
	$HTTPRequest.request(Network.webServer + "/match", headers, true, HTTPClient.METHOD_POST, query)
	
	yield($HTTPRequest, "request_completed")
	
	rpc("switch_to_game")


sync func switch_to_game() -> void:
	
	for child in PersistentNodes.get_children():
		if child.is_in_group("Player"):
			child.update_shoot_mode(true)
	
	
	get_tree().change_scene("res://Game.tscn")


func _on_HTTPRequest_request_completed(_result, _response_code, _headers, body):
	
	
	if body.empty():
		return
		
	var json = JSON.parse(body.get_string_from_utf8())


func _on_BackToLobby_pressed():
	get_tree().network_peer = null
	Global.players = []
	for player in PersistentNodes.get_children():
		player.queue_free()
	get_tree().change_scene_to(lobbyScene)
