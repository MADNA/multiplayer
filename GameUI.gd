extends CanvasLayer

onready var win_timer = $Control/Winner/WinTimer
onready var winner = $Control/Winner

var player_winner:bool

func _ready() -> void:
	player_winner = false
	winner.hide()

func _process(_delta:float) -> void:
	if Global.alive_players.size() <= 1 and get_tree().has_network_peer():
		if Global.alive_players[0].name == str(get_tree().get_network_unique_id()):
			winner.show()
			if !player_winner and $HTTPRequest.get_http_client_status() == 0:
				var headers = ["Content-Type: application/json", "token: " + User.user_file.token]
				$HTTPRequest.request(Network.webServer + "/" + User.user_file._id + "/victory", headers, true, HTTPClient.METHOD_PUT)
				yield($HTTPRequest, "request_completed")
				player_winner = true
		
		if win_timer.time_left <= 0:
			win_timer.start()



