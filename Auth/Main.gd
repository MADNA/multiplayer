extends Control

var can_pass:bool = false
onready var serverScene = preload("res://NetworkSetup.tscn")
onready var updateScene = preload("res://Auth/Update.tscn")

func _ready():
	
	$Tween.interpolate_property(self, "modulate", Color(0, 0, 0, 0), Color(1, 1, 1, 1), 1)
	$Tween.start()
	$UserConfigure/ServerButton.hide()
	
	if User.user_file:
		if User.verified_user:
			$ProfileContainer.show()
			$UserConfigure/ServerButton.show()
			$ProfileContainer/UpdateButton.text = User.user_file.username
			return
		if User.user_file.token:
			$HTTPRequest.request(Network.webServer + "/login/verify", ["Content-Type: application/json", "token: " + User.user_file.token], false, HTTPClient.METHOD_GET)
			yield($HTTPRequest, "request_completed")

func _process(_delta: float):
	if $HTTPRequest.get_http_client_status() == 6:
		$MarginContainer/Register/AuthRegister.disabled = true
	else:
		$MarginContainer/Register/AuthRegister.disabled = false



func _on_Register_pressed():
	$MarginContainer/Register.visible = true
	$MarginContainer/Login.visible = false


func _make_post_request(url, data_to_send, use_ssl):
	# Convert data to json string:
	var query = JSON.print(data_to_send)
	# Add 'Content-Type' header:
	var headers = ["Content-Type: application/json"]
	$HTTPRequest.request(url, headers, use_ssl, HTTPClient.METHOD_POST, query)


func _on_Login_pressed():
	$MarginContainer/Login.visible = true
	$MarginContainer/Register.visible = false
	$UserConfigure.hide()
	$MarginContainer.show()
	$Back.show()


func _on_AuthLogin_pressed():
	if $MarginContainer/Login/User.text == "" or $MarginContainer/Login/Password.text == "":
		return
	var query = {
		"email": $MarginContainer/Login/User.text,
		"password": $MarginContainer/Login/Password.text
	}
	
	_make_post_request(Network.webServer + "/login", query, false)


func _on_HTTPRequest_request_completed(_result, _response_code, _headers, body):
	if body.empty():
		return
	var json = JSON.parse(body.get_string_from_utf8())
	if json.result:
		if json.result.ok == false:
			var message = json.result.err.message
			show_warn(message)
		else:
			if(json.result.user):
				User.save_game(json.result)
				User.verified_user = true
				$UserConfigure/ServerButton.show()
				$ProfileContainer.show()
				$ProfileContainer/UpdateButton.text = json.result.user.username
			show_warn(json.result.message)


func show_warn(text):
	$AcceptDialog.show()
	$AcceptDialog.dialog_text = text


func _on_AuthRegister_pressed():
	if ($MarginContainer/Register/Name.text == "" or $MarginContainer/Register/User.text == "" or $MarginContainer/Register/Email.text == "" or $MarginContainer/Register/Password.text == ""):
		show_warn("Rellene todos los campos")
		return
	
	if $MarginContainer/Register/Name.text.length() < 2:
		show_warn("El nombre es muy corto")
		return
	
	if $MarginContainer/Register/User.text.length() < 4:
		show_warn("El nombre de usuario es muy corto")
		return
	
	if $MarginContainer/Register/Email.text.length() < 11:
		show_warn("Inserte un email correcto")
		return
	
	if $MarginContainer/Register/Password.text.length() < 6:
		show_warn("La contraseña es muy corta")
		return
	
	if $MarginContainer/Register/Name.text.length() > 30:
		show_warn("El nombre es muy largo")
		return
	
	if $MarginContainer/Register/User.text.length() > 20:
		show_warn("El nombre de usuario es muy largo")
		return
	
	if $MarginContainer/Register/Email.text.length() > 50:
		show_warn("Inserte un email correcto")
		return
	
	if $MarginContainer/Register/Password.text.length() > 30:
		show_warn("La contraseña es muy larga")
		return
	
	var query = {
		"name": $MarginContainer/Register/Name.text,
		"username": $MarginContainer/Register/User.text,
		"email": $MarginContainer/Register/Email.text,
		"password": $MarginContainer/Register/Password.text
	}
	
	_make_post_request(Network.webServer + "/register", query, false)
	$MarginContainer/Register/AuthRegister.disabled = true


func _on_CreateUser_pressed():
	$MarginContainer.visible = true
	$MarginContainer/Register.visible = true
	$UserConfigure.visible = false
	$Back.show()


func _on_Back_pressed():
	if $MarginContainer/Login.visible:
		$MarginContainer/Login.hide()
		$MarginContainer.hide()
		$UserConfigure.show()
		$Back.hide()
	elif $MarginContainer/Register.visible:
		$MarginContainer/Register.hide()
		$MarginContainer.hide()
		$UserConfigure.show()
		$Back.hide()


func _on_ServerButton_pressed():
	# get_node("/root/Main").queue_free()
	# get_tree().get_root().add_child(serverScene.instance())
	get_tree().change_scene_to(serverScene)


func _on_UpdateButton_pressed():
	get_tree().change_scene_to(updateScene)
