extends Control

signal can_pass
signal reject

var user_file
var headers
var user_token

func _process(_delta):
	if $HTTPRequest.get_http_client_status() == 6:
		$Update/HBoxContainer/Update.disabled = true
		$ChangePasswordButton.disabled = true
		$MarginContainer/ChangePassword/OldPassword/VerifyPassword.disabled = true
	else:
		$Update/HBoxContainer/Update.disabled = false
		$ChangePasswordButton.disabled = false
		$MarginContainer/ChangePassword/OldPassword/VerifyPassword.disabled = false

func _ready():
	User.load_game()
	update_stats()
	
	$Tween.interpolate_property(self, "modulate", Color(0, 0, 0, 0), Color(1, 1, 1, 1), 1)
	$Tween.start()
	user_file = User.user_file
	
	if user_file:
		$Update/Name.text = user_file.name
		$Update/Email.text = user_file.email
		$Update/Username.text = user_file.username
		user_token = user_file.token
		headers = ["Content-Type: application/json", "token: " + user_token]

func show_warn(text):
	$AcceptDialog.show()
	$AcceptDialog.dialog_text = text

func update_stats():
	
	var victories = str(User.user_file.victories)
	var losses = str(User.user_file.losses)
	var matches = str(User.user_file.match)
	$MarginContainer/RichTextLabel.bbcode_text = "[right]Victorias: [color=green]" + victories + "[/color]\n \n Derrotas: [color=red]" + losses + "[/color]\n \n Empates: [color=maroon]" + matches + "[/color] [/right]"


func _on_ChangePasswordButton_pressed():
	$Update.hide()
	$MarginContainer/ChangePassword.show()


func _on_Update_pressed():
	
	if $Update/Name.text.empty() or $Update/Username.text.empty() or $Update/Email.text.empty():
		show_warn("Por favor rellene todos los campos")
		return
	
	if $Update/Name.text.length() < 2:
		show_warn("El nombre es muy corto")
		return
	
	if $Update/Email.text.length() < 11:
		show_warn("Inserte un email correcto")
		return
	
	if $Update/Username.text.length() <= 1:
		show_warn("El nombre de usuario es muy corto")
	
	var url = Network.webServer + "/" + User.user_file.id
	var _query = {
		"name": $Update/Name.text,
		"username": $Update/Username.text,
		"email": $Update/Email.text
	}
	
	var query = JSON.print(_query)
	
	
	$HTTPRequest.request(url, headers, true, HTTPClient.METHOD_PUT, query)


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var json = JSON.parse(body.get_string_from_utf8())
	if json.result.message:
		User.save_game(json.result)
		show_warn(json.result.message)


func _on_BackToLobby_pressed():
	get_tree().change_scene_to(load("res://Auth/Main.tscn"))


func _on_VerifyPassword_pressed():
	if $MarginContainer/ChangePassword/OldPassword/LineEdit.text == "":
		return
	
	var url = Network.webServer + "/" + User.user_file.id
	
